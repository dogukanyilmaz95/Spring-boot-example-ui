import React from 'react';
import ReactTable from 'react-table'
import Modal from 'react-responsive-modal';
import 'react-table/react-table.css';


const user = (props) => {

    let users = [];

    for (let i = 0; i < props.userList.length; i++) {
        users.push(
            {
                name: props.userList[i].name,
                surname: props.userList[i].surname,
                email: props.userList[i].email,
                id: props.userList[i].id,
            }
        );
    }

    const onRowClick = (state, rowInfo) => {
        return {
            onClick: e => {
                const row = rowInfo.original;
                if ((e.target.classList[1] && e.target.classList[1].toString() === "glyphicon-trash") ||
                    (e.target.classList[3] && e.target.classList[1].toString() === "deleteBtn")) {
                    props.onDeleteOpenModal(row.id);
                }
                else if ((e.target.classList[1] && e.target.classList[1].toString() === "glyphicon-pencil") ||
                    (e.target.classList[3] && e.target.classList[1].toString() === "editBtn")) {
                    props.onOpenModal(
                        row.name,
                        row.surname,
                        row.email,
                        row.id);
                }
            }
        }
    };


    const columns = [
        {
            columns: [
                {
                    Header: "Name",
                    accessor: "name"
                },
                {
                    Header: "Surname",
                    accessor: 'surname'
                },
                {
                    Header: "Email",
                    accessor: "email"
                },
                {
                    expander: true,
                    Header: () => <strong>Edit</strong>,
                    width: 65,
                    Expander: ({isExpanded, ...rest}) =>
                        <div>
                            <button className="btn btn-primary btn-xs editBtn" onClick={onRowClick}>
                                <span className="glyphicon glyphicon-pencil"></span>
                            </button>
                        </div>,
                    style: {
                        cursor: "pointer",
                        textAlign: "center"
                    }
                },
                {
                    expander: true,
                    Header: () => <strong>Delete</strong>,
                    width: 65,
                    Expander: ({isExpanded, ...rest}) =>
                        <div>
                            <button className="btn btn-danger btn-xs deleteBtn" onClick={onRowClick}>
                                <span className="glyphicon glyphicon-trash"></span>
                            </button>
                        </div>,
                    style: {
                        cursor: "pointer",
                        textAlign: "center"
                    }
                }
            ]
        }
    ]


    const pageSize = 5;
    return (
        <div>
            <h2>User Management</h2>
            <div className="row">
                <div className="col-md-12">
                    <button className="btn btn-primary" onClick={props.onCreateOpenModal}>New User</button>
                    <div className="table-responsive" style={{marginTop: '20px'}}>
                        <ReactTable
                            defaultPageSize={pageSize}
                            data={users}
                            columns={columns}
                            filterable
                            getTrProps={onRowClick}
                        />
                        <div className="clearfix"></div>
                    </div>
                </div>
            </div>
            <div className="modal fade" id="edit" ref={(div) => {
                this.editModal = div;
            }} tabIndex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
            </div>

            <Modal style={{padding: "20px;"}} open={props.open} onClose={props.onCloseModal} little>
                <h3 style={{paddingTop: "15px", paddingBottom: "15px"}}>Edit Your User Please</h3>
                <div className="form-group">
                    <input className="form-control" type="text" placeholder="Email" onChange={props.email}
                           value={props.emailEdit}/>
                </div>
                <div className="form-group">
                    <input className="form-control " type="text" placeholder="name" onChange={props.name}
                           value={props.nameEdit}/>
                </div>
                <div className="form-group">
                    <input className="form-control " type="text" placeholder="surname" onChange={props.surname}
                           value={props.surnameEdit}/>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-warning btn-lg" onClick={props.submitEditForm}
                            style={{width: '100%'}}><span className="glyphicon glyphicon-ok-sign"></span> Save
                    </button>
                    {props.formValid === false ?
                        <p style={{"color": "red", "textAlign": "center"}}>{props.errorMessage}</p> : ''
                    }
                </div>
            </Modal>


            <Modal style={{padding: "20px;"}} open={props.openCreateModal} onClose={props.onCreateCloseModal} little>
                <h3 style={{paddingTop: "15px", paddingBottom: "15px"}}>Please Enter Your new User</h3>
                <div className="form-group">
                    <input type="email" className="form-control" placeholder="Email" onChange={props.email} required/>
                </div>
                <div className="form-group">
                    <input className="form-control " type="text" placeholder="name" onChange={props.name}/>
                </div>
                <div className="form-group">
                    <input className="form-control " type="text" placeholder="surname" onChange={props.surname}/>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-warning btn-lg" onClick={props.submitForm}
                            style={{width: '100%'}}><span className="glyphicon glyphicon-ok-sign"></span> Create
                    </button>
                    {props.formValid === false ?
                        <p style={{"color": "red", "textAlign": "center"}}>{props.errorMessage}</p> : ''
                    }
                </div>
            </Modal>


            <Modal style={{padding: "20px;"}} open={props.openDeleteModal} onClose={props.onDeleteCloseModal} little>
                <h3 style={{paddingTop: "15px"}}>Delete this User?</h3>
                <p>are you sure you want to delete this User</p>
                <div style={{textAlign: "center"}}>
                    <button style={{marginRight: "10px"}} className="btn btn-danger" onClick={props.deleteUser}>Yes
                    </button>
                    <button className="btn btn-secondary" onClick={props.onDeleteCloseModal}>No</button>
                </div>
            </Modal>
        </div>
    );
};

export default user;

