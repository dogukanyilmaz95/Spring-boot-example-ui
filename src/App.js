import React, { Component } from 'react';
import logo from './logo.svg';
import {Route,Switch} from 'react-router-dom';
import './App.css';
import UserPage from "./container/userPage/UserPage";

class App extends Component {
    render() {
        return (
            <div>
                <Switch>
                    <Route path="/"  exact component={UserPage}/>
                </Switch>
            </div>
        );
    }
}

export default App;
