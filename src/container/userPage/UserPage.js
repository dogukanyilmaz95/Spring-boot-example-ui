import React, {Component} from 'react';
import User from '../../components/User/User'

let baseConfig = "http://localhost:8080/";

class UserPage extends Component {


    state = {
        users: "",
        openMenu: true,
        email: "",
        surname: "",
        name: "",
        id: "",
        openEditModal: false,
        openCreateModal: false,
        openDeleteModal: false,
        submitTheForm: false,
        errorMessage: "",
        emailValid: false,
        nameValid: false,
        surnameValid: false,
    };

    componentDidMount = () => {
        this.getAllUser();
    };

    emailChangeHandler = (event) => {
        if(event.currentTarget.value){
            this.setState({
                email : event.currentTarget.value,
            },function () {
                this.emailValidateHandler();
            })
        }
        else{
            this.setState({
                email : "",
            },function () {
                this.emailValidateHandler();
            })
        }
    };

    emailValidateHandler = () => {
        if (this.state.email.toString().indexOf("@") !== -1 && this.state.email.toString().indexOf(".") !== -1 && this.state.email.toString() !== ""
            && this.state.email.toString().length < 45) {
            this.setState({
                emailValid : true,
                errorMessage : "",
            })
        }
        else {
            this.setState({
                emailValid : false,
                errorMessage : "Please Enter your real email",
            })
        }
    };

    nameChangeHandler = (event) => {
        if(event.currentTarget.value){
            this.setState({
                name : event.currentTarget.value,
            })
        }
        else{
            this.setState({
                name: "",
            })
        }
        this.nameValidateHandler();
    };

    nameValidateHandler = () => {
        if (this.state.name.length.toString() > 2 && this.state.name.length.toString() < 25) {
            this.setState({
                nameValid : true,
                errorMessage : "",
            }),function () {

            }
        }
        else {
            this.setState({
                nameValid : false,
                errorMessage : "Please Enter Your Name",
            })
        }
    };


    surnameChangeHandler = (event) => {
        if(event.currentTarget.value){
            this.setState({
                surname:event.currentTarget.value,
            })
        }
        else{
            this.setState({
                surname : "",
            })
        }
        this.surnameValidateHandler();
    };

    surnameValidateHandler = () => {
        if (this.state.surname.length.toString() > 2 && this.state.surname.length.toString() < 30) {
            this.setState({
                surnameValid : true,
                errorMessage : "",
            })
        }
        else {
            this.setState({
                surnameValid : false,
                errorMessage : "Please Enter Your Surname",
            })
        }
    };

    formValidate = () => {
        if (this.state.emailValid === true && this.state.surnameValid === true && this.state.nameValid === true) {
            this.state.submitTheForm = true;
        }
        else {
            this.state.submitTheForm = false;
        }
    };

    getAllUser = () => {
        return fetch(baseConfig + "user/getAll", {
            method: 'GET',
        }).then((data) => data.json())
            .then(
                (data) => {
                    this.setState({users: data})
                }
            )
            .catch(
                (err) => {
                    console.log("Error");
                }
            )
    };

    onCloseModal = () => {
        this.setState({openEditModal: false});
        this.clearModalData();
    };

    onCreateOpenModal = () => {
        this.setState({openCreateModal: true});
    };

    onCreateCloseModal = () => {
        this.setState({openCreateModal: false});
        this.clearModalData();
    };

    onDeleteOpenModal = (id) => {
        this.setState({openDeleteModal: true, id: id});
    };

    onDeleteCloseModal = () => {
        this.setState({openDeleteModal: false});
    };

    clearModalData = () => {
        this.setState({
            email : "" ,name : "" ,surname : "",errorMessage:"",submitTheForm:false
        })
    };

    onOpenModal = (name, surname, email, id) => {
        this.setState({openEditModal: true});
        this.state.surname = surname;
        this.state.email = email;
        this.state.name = name;
        this.state.id = id;
        this.emailValidateHandler();
        this.surnameValidateHandler();
        this.nameValidateHandler();
    };

    updateUser = () => {
        this.formValidate();
        const checkValidate = this.state.submitTheForm;
        console.log(this.state.addressValid + " " + this.state.citizenIdValid + " " + this.state.nameValid + " " + this.state.phoneValid + " " +
            this.state.surnameValid + " " + this.state.emailValid)
        if (checkValidate === false) {
            console.log("false");
        }
        else {
            return fetch(baseConfig + "user/", {
                method: 'PUT',
                body: JSON.stringify({
                    id: this.state.id,
                    email: this.state.email.toString(),
                    name: this.state.name.toString(),
                    surname: this.state.surname.toString(),
                }),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then((data) => data.json())
                .then(
                    (data) => {
                        console.log("DONE");
                        this.getAllUser();
                        this.onCloseModal();
                        // notify.show("İşlemi Başarılı", "success");
                        console.log(data);
                    }
                )
                .catch(
                    (err) => {
                        console.log("Error");
                    }
                )
        }
    };

    createUser = () => {
        this.formValidate();
        const checkValidate = this.state.submitTheForm;
        console.log(this.state.addressValid + " " + this.state.citizenIdValid + " " + this.state.nameValid + " " + this.state.phoneValid + " " +
            this.state.surnameValid + " " + this.state.emailValid)
        if (checkValidate === false) {
            console.log("false");
        }
        else {
            return fetch(baseConfig + "user/create", {
                method: 'POST',
                body: JSON.stringify({
                    email: this.state.email.toString(),
                    name: this.state.name.toString(),
                    surname: this.state.surname.toString(),
                }),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then((data) => {
                console.log(data);
                this.getAllUser();
                this.onCreateCloseModal();
                // notify.show("İşlemi Başarılı", "success");
            }).catch(
                (err) => {
                    console.log("Error");
                }
            )
        }
    };

    deleteUser = () => {

        return fetch(baseConfig + "user/", {
            method: 'DELETE',
            body: JSON.stringify({
                id: this.state.id.toString(),
            }), headers: {
                'Content-Type': 'application/json'
            }

        }).then((data) => {
                console.log("DONE");
                this.getAllUser();
                this.onDeleteCloseModal();
                // notify.show("Silme İşlemi Başarılı", "error");
            }
        )
            .catch(
                (err) => {
                    console.log("eeeerrroooor");
                }
            )
    };

    render() {

        return (
            <User
                userList={this.state.users}
                onDeleteOpenModal={this.onDeleteOpenModal}
                onDeleteCloseModal={this.onDeleteCloseModal}
                onOpenModal={this.onOpenModal}
                onCreateOpenModal={this.onCreateOpenModal}
                onCreateCloseModal={this.onCreateCloseModal}
                onCloseModal={this.onCloseModal}
                submitEditForm={this.updateUser}
                submitForm={this.createUser}
                deleteUser={this.deleteUser}
                open={this.state.openEditModal}
                openCreateModal={this.state.openCreateModal}
                openDeleteModal={this.state.openDeleteModal}
                emailEdit={this.state.email}
                nameEdit={this.state.name}
                surnameEdit={this.state.surname}
                email={this.emailChangeHandler}
                name={this.nameChangeHandler}
                surname={this.surnameChangeHandler}
                errorMessage = {this.state.errorMessage}
                formValid={this.state.submitTheForm}
            />
        )

    }
}

export default UserPage;